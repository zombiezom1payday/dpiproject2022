/**
 * A function to get additional crq form (crq  number, components fields and dellete button)
 * @param {*} counter : stores the number of the crq continer 
 */
function get_additional_crq(counter){
    //get the additional crq form form addCRQ.html
    $.get('../Html/addCRQ.html', function(data){
        
        // append the html form into an existing container in dpi.html
        $(".new-crq").append(data);
        
        //if the new form does not contain id, add new id tag and remove existing class
        if (!$(".newcrq").attr('id')){

            $("div.newcrq").attr('id', 'newCRQ'+counter);
            $('input.crqInput').attr('id', 'crqNumber'+counter);

            $("div").removeClass("newcrq");
            $("input").removeClass("crqInput");

            //call this function to get components list
            get_components_list(counter);
        }
    });
}

/**
 * this function will display the component list according to the selected app
 * 
 * @param {*} counter : number of crq continer
 */
function get_components_list(counter){
    
    // get the components array from globalVars.js
    const components = JSON.parse(localStorage.getItem('components'));

    //get the selected app name
    let app_name = $('#app-opt').find('option:selected').text();
   
    //get the dropdown container
    let drop_down_list = $("#newCRQ"+counter).find("#components-dropdown");

    //check for undefiend dropdowns which are related to deleted coniners
    if(drop_down_list != undefined){

        // this loop wil find the components for the selected app name
        for (let component_key of Object.keys(components)) {
            //if the key/app name in the array match the selected app name
            if(component_key == app_name){
                //variable to store the html elements
                let comp_elements = "";
                //loop throught the components 
                for(component of components[app_name]){
                    //create a new html tag for each component
                    comp_elements += "<a class='dropdown-item'><input type='checkbox' value='"+component+"'> "+component+"</a>";
                }
                // add/display all the components elements in the dropdown
                drop_down_list.html(comp_elements);
            }
        }// end of for loop
    }// end of if satement
}// end of function


/**
 * This function will add input fields (build, repo, branch, and tag) when a component is clicked
 * 
 * @param {*} parent_div : parent contianer id
 * @param {*} checked_value : selected component
 * @param {*} is_checked : boolean value true/false
 */
function add_build_field (parent_div, checked_value, is_checked){
    // find the empty continer in addCRQ.html thal will be used to load the build continers
    let biuld_container = $("#"+parent_div).find("#build-div");
    /**
     * this variable will store the checked value with all the spaces removed
     * and wil be used as a class value to the html input fields
    */
    let input_class = checked_value.split(' ').join('');

    // if the value is checked
    if(is_checked){
        // create build text area and label
        let build_label = '<br><label class="form-control-label h5 px-3 '+ parent_div+'-repo-'+ input_class+'">'+ checked_value +'</label>';
        let build_textarea = '<textarea class="form-control '+ parent_div+'-build-'+ input_class+'" id="exampleFormControlTextarea1" rows="3" value=""></textarea>';
        
        // create repository input field and label
        let repo_label = '<label class="form-control-label px-3 '+ parent_div+'-repo-'+ input_class+'"> Repository</label>';
        let repo_input = ' <input type="text" class="'+ parent_div+'-repo-'+ input_class+'" value=""> ';
        
        // create branch inut field and label
        let branch_label = '<label class="form-control-label px-3 '+ parent_div+'-repo-'+ input_class+'">Branch</label>';
        let branch_input = ' <input type="text" class="'+ parent_div+'-branch-'+ input_class+'" value=""> ';
        
        // create tag inout field and label
        let tag_label = '<label class="form-control-label px-3 '+ parent_div+'-repo-'+ input_class+'">Tag</label>';
        let tag_input = ' <input type="text" class="'+ parent_div+'-tag-'+ input_class+'" value="">';

        // apped all the html field to the empty container in addCRQ.html
        biuld_container.append(build_label + build_textarea + repo_label + repo_input + branch_label + branch_input + tag_label + tag_input);
    }
    // if the value is unchecked 
    else{
        // remove all the displayed html fields (build, repo, tag and branch fields)
        $("."+parent_div+'-'+ input_class).remove();
        $("."+parent_div+'-build-'+ input_class).remove();
        $("."+parent_div+'-repo-'+ input_class).remove();
        $("."+parent_div+'-branch-'+ input_class).remove();
        $("."+parent_div+'-tag-'+ input_class).remove();
    }
}


/**
 * this function will save or create a DPI according to the call type 
 * passed to this function
 * 
 * @param {*} content : DPI content in json format 
 * @param {*} callType : API call type
 * @param {*} release_id : DPI release id
 */
function Save_DPI_Content(content, callType, release_id, app_name){
    //the main part of api url, accesss token, branch_name and proect IDs retrieved from globalVars.js 
    let urlMain = localStorage.getItem('urlMain');
    let token = localStorage.getItem('tokenGlobal');
    let branch = localStorage.getItem('branch_name');
    const proj_IDs =  JSON.parse(localStorage.getItem('proj_IDs'));
    let projId = 0;

    // find the project id equal to the selected app
    for (let app of Object.keys(proj_IDs)) {
        if(app == app_name){
            projId = proj_IDs[app];
        }
    }

    // itialize the api call elements
    var settings = {
        "url": urlMain+projId+"/repository/files/"+release_id+".json",
        "method": callType,
        "contentType": "application/json;charset=utf-8",
        "timeout": 0,
        "headers": {
          "Authorization": "Bearer " + token
        },
        "data": JSON.stringify({
            branch:branch,
            commit_message: "DPI Commit",
            content: content,
        }),
    };
      
    // make the ajax call
    $.ajax(settings)
    .done(function (response) {
        //if updating successed, reload the page
        if(callType == 'PUT'){
            setTimeout(function(){
                location.reload();
            }, 1000);
        }
        //if creating new DPI successed, display a message and pp-up link
        else{
            $('p.message').append('New DPI Successfuly uploaded to GitLab!');
            //call this function to display the pop-up
            show_DPI_popup(release_id,projId);
        }
    })
    .fail(function (jqXHR, textStatus, errorThrown) { 
        //display a message if the api call did not successed
        $('p.message').text('Something went wrong while uploading DPI to GitLab!, Contact the admin');
    });
}//end of function

/**
 * this fnction will get an existing DPI content from GitLab
 */
function Get_Dpi_Content(){
    // get all the html form fields
    let releaseId = $("#releaseID");
    let appName = $('#app-opt');
    let releaseDate = $("#releaseDate");
    let coordName = $("#coordName");
    let coordEmail = $("#coordEmail");
    let releaseStatus = $("#status-opt");

    //get the file name and its path from DpiList.js
    let file_name = localStorage.getItem('file-name');

    //dislay file name above the html form 
    $("#file-name").text(file_name.split('.')[0]);

    //get the app name from apps.js
    let app_name = localStorage.getItem('app-name');

    //get the main part of api url, branch name and projects IDS from Main.js
    let urlMain = localStorage.getItem('urlMain');
    let branch = localStorage.getItem('branch_name');
    const proj_IDs =  JSON.parse(localStorage.getItem('proj_IDs'));
    let projId = 0;

    // find the project id equal to the selected app
    for (let app of Object.keys(proj_IDs)) {
        if(app == app_name){
            projId = proj_IDs[app];
        }
    }

    // ajax call to gitlab api 
    $.ajax({
        url: urlMain + projId + "/repository/files/"+file_name+"/raw?ref="+branch,
        method: "GET",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        success: function(data) {
            //if the call successed, add and display the data in DPI form
            releaseId.val(data['release_id']);
            appName.val(data['application_name']);
            releaseDate.val(data['date'])
            coordName.val(data['coordinate_name']);
            coordEmail.val(data['coordinate_email']);
            releaseDate.val(data['date']);
            releaseStatus.val(data['status']);

            //get the crq list
            let crqData = data['crq_list'];

            //loop through the list
            for(let i = 0; i < crqData.length; i++){
                // call th function to get the html fields for each crq
                get_additional_crq(i+1); 
                
                // setTimeout to delay the below implementation unitl crq fields is created 
                setTimeout(function(){
                    //dislay the crq number
                    $("#crqNumber"+(i+1)).val(crqData[i]['crq_number']);
                  
                    //find the comonents list
                    let componentList = crqData[i]['project_components'];

                    for(index in componentList)
                    {
                        //find each checkbox in the components dropdown 
                        $("#newCRQ"+(i+1)).find('input[type="checkbox"]').each(function(e){
                            //if the value is equal to the value in the retrieved data and it's not 'No Component/s'
                            if($(this).val() == componentList[index]['component'] && componentList[index]['component'] != 'No component/s'){
                                //check off the components
                                $(this).prop("checked", true);

                                //call this funtion to dispal the builds fields (build, repo, tag and brach)
                                add_build_field ("newCRQ"+(i+1), $(this).val(), true)

                                /**
                                 * this variable will store the checked value with all the spaces removed
                                 * and wil be used as a class value to the html input fields
                                */
                                let input_class = $(this).val().split(' ').join('');

                                // find all the builds fields and append the value to their respective fields 
                                $("textarea.newCRQ"+(i+1)+"-build-"+ input_class).append(componentList[index]['build']);
                                $("input.newCRQ"+(i+1)+"-repo-"+ input_class).val(componentList[index]['repo_name']);
                                $("input.newCRQ"+(i+1)+"-branch-"+ input_class).val(componentList[index]['branch_name']);
                                $("input.newCRQ"+(i+1)+"-tag-"+ input_class).val(componentList[index]['tag']);
                            }
                            // if the component value is 'No Component/s'
                            else if(componentList[index]['component'] == 'No component/s'){
                                //check off the field
                                $(this).prop("checked", true);

                                //from the parent container, find the checkboxes
                                $("#newCRQ"+(i+1)).find('input[type="checkbox"]').each(function(e){
                                    //find the other values (not 'no components')
                                    if($(this).val() != "No component/s"){
                                        //uncheck and disable the checkboxes
                                        $(this).prop("checked", false);
                                        $(this).attr('disabled', true);
                                    }
                                });
                            }// end of if staement
                        });// end of find function 
                    }// end of for loop
                }, 500);// end of set timeout
            }// end of for oop
        },
        error: function(request, msg, error){
            //display a message if the api call did not successed
            $('p.message').text('Something went wrong while uploading DPI to GitLab!, Contant the admin');
        }
    });// end of function
}

/**
 * A function to dispaly dpi link in a pop-up
 * 
 * @param {*} release_id : dpi release id 
 * @param {*} projId : repositoy id
 */
function show_DPI_popup(release_id,projId) {
    // variable to store the dapi file link in gitlab
    var myLink = "";

    // create a link according to the selected project
    if(projId == 140){
        myLink = "https://intra.gitlab.devops.css.gov.on.ca/deployments/dscis-dpi/-/blob/main/"+release_id+".json";
    }
    else if(projId == 145){
        myLink = "https://intra.gitlab.devops.css.gov.on.ca/deployments/sams-dpi/-/blob/main/"+release_id+".json";
    }
    else if(projId == 138){
        myLink = "https://intra.gitlab.devops.css.gov.on.ca/deployments/cpin-dpi/-/blob/main/"+release_id+".json";
    }
    else if(projId == 141){
        myLink = "https://intra.gitlab.devops.css.gov.on.ca/deployments/fcms-dpi/-/blob/main/"+release_id+".json";
    }
    else if(projId == 142){
        myLink = "https://intra.gitlab.devops.css.gov.on.ca/deployments/hcd-dpi/-/blob/main/"+release_id+".json";
    }
    else if(projId == 143){
        myLink = "https://intra.gitlab.devops.css.gov.on.ca/deployments/mftis-dpi/-/blob/main/"+release_id+".json";
    }
    
    // display the pop-up with the link
    alert(myLink);
}