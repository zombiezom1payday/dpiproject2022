/**
 * this file contains click events for the navigation bar
 * and the main menu items 
 * it will be imported in all .js files
 * 
 * it will be linked to Main.html
*/
$(document).ready(function(e){ 
    /**click events for naigation bar items
     * each item will pass a navigation type that will be used in dpi1.js and dpi2.js
    */
    $("#nav-menu").on('click',function(e){
        window.location.href = "../Html/Main.html";
    });

    $("#nav-new-dpi").on('click',function(e){
        localStorage.setItem('navigation-type', 'new');
        window.location.href = "../Html/dpi.html";
    });

    $("#nav-dpi-history").on('click',function(e){
        localStorage.setItem('navigation-type', 'history');
        window.location.href = "../Html/apps.html";
    });

    $("#nav-edit-dpi").on('click',function(e){
        localStorage.setItem('navigation-type', 'modify');
        window.location.href = "../Html/apps.html";
    });


    /**click event for the main menu items
     * each item will pass a navigation type that will be used in dpi1.js and dpi2.js
    */
    $("#menu-new-dpi").on('click',function(e){
        localStorage.setItem('navigation-type', 'new');
        window.location.href = "../Html/dpi.html";
    });

    $("#menu-dpi-history").on('click',function(e){
        localStorage.setItem('navigation-type', 'history');
        window.location.href = "../Html/apps.html";
    });

    $("#menu-edit-dpi").on('click',function(e){
        localStorage.setItem('navigation-type', 'modify');
        window.location.href = "../Html/apps.html";
    });
});