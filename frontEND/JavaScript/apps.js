/**
 * this file contains click functionality for the app names that will be displayed
 * before retrieving a list of DPI files
 * 
 * it will be linked to apps.html
*/
$(document).ready(function(e){

    /** this variable will get the navigation type from Main.js
     * the value will present which button the user clicked
     * ex: if user clicked (NEW DPI) the value will be 'new'
    */
    let navigation_type = localStorage.getItem('navigation-type');

    //change page title according to navigation type
    if (navigation_type == 'history'){
        $("h2.dpi-title").text("DPI History");
    }
    else if (navigation_type == 'modify'){
        $("h2.dpi-title").text("Modify DPI");
    }

    //click event for each app name 
    $("a.app-item").on('click',function(e){
        e.preventDefault();
        //get the clicked app name
        let app_name = e.currentTarget.innerText;
        //set the clicked app name in a local storage
       localStorage.setItem('app-name', app_name);
        //redirect to DPI list page
       window.location.href = "../Html/dpiList.html";
    });
});