/**
 * this file contains all the global variables that will be mainly used in all .js files
*/

//production repo link
var urlMain = "https://intra.gitlab.devops.css.gov.on.ca/api/v4/projects/";

//production repo token
var tokenGlobal_ON = "glpat-9CsWv689A6W6VXJBzY6G";

//main branch name
var branch_name_global = "main";

//repsitories id
const proj_IDs = {
    'DSCIS' : 140, 

    'SAMS' : 145, 

    'CPIN' : 138,

    'FCMS' : 141,

    'HCD' :  142,

    'MFTIS' : 143
};


/**
* key list/array for each project component
* the components wll be displayed according to the selected project/app name
*/
const components = {
    'DSCIS' : ['No component/s', 'Curam', 'Congnos'], 

    'SAMS' : ['No component/s', 'Curam', 'Emergency Assistance - EA', 'SAMS Cognos', 'Inforatica Reporting - ETL',
              'Informatica Interface - ETL', 'EST - EPA-Bil', 'My Benefits - SA Mobile', 'Messaging - 2WM', 'SADA'], 

    'CPIN' : ['No component/s', 'Curam', 'Curam DM', 'Cognos', 'FM', 'CAMS', 'Informatica Reporting',
              'Informatica DM'],

    'FCMS' : ['No component/s', 'eBS', 'FRO Online', 'Siebel', 'BPM', 'BI Publisher', 'OPA'],

    'HCD' :  ['No component/s', 'HCD-ISCIS - Core Application', 'HCD-ISCIS Informatica and Database',
              'Cognos-IRSS'],

    'MFTIS' : ['No component/s', 'Curam', 'FaxOPS']
};


//set all the global value in local storage to be used in other .js files
localStorage.setItem("urlMain", urlMain);
localStorage.setItem("tokenGlobal", tokenGlobal_ON);
localStorage.setItem("branch_name", branch_name_global);
localStorage.setItem("proj_IDs", JSON.stringify(proj_IDs));
localStorage.setItem("components", JSON.stringify(components));


/**
APIs for  ACTUAL PROJECT 
access token : glpat-P9j1CxATBNfo4FDf5SQP

POST - create new DPI - DONE
https://intra.stage.gitlab.devops.css.gov.on.ca/api/v4/projects/[PROJECT-ID]/repository/files/[RELEASE-ID].json?branch=main&commit_message="create new file"&content={"name":"Stephan"}

GET - paginated files list - DONE
https://intra.stage.gitlab.devops.css.gov.on.ca/api/v4/projects/[PROJECT-ID]/repository/tree?path=jsonData&ref=Nora-Branch&per_page=3&page=1

GET - file content -DONE
https://intra.stage.gitlab.devops.css.gov.on.ca/api/v4/projects/[PROJECT-ID]/repository/files/[FILE-NAME]/raw?ref=main

PUT - update file - DONE
https://intra.stage.gitlab.devops.css.gov.on.ca/api/v4/projects/[PROJECT-ID]/repository/files/[FILE-NAME]?branch=main&commit_message="create new file"

GET - searching - DONE
https://intra.stage.gitlab.devops.css.gov.on.ca/api/v4/projects/[PROJECT-ID]/repository/files/[FILE-NAME]?ref=main
 */