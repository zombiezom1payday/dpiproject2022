/**
 * this file contains all the functionality of creating and modify DPI
 * this file will be linked to dpi.html
*/
$(document).ready(function() {
    
    /** 
     * this variable will get the navigation type from Main.js
     * the value will present which button the user clicked
     * ex: if user clicked (NEW DPI) the value will be 'new'
     */
    let navigation_type = localStorage.getItem('navigation-type');
    
    /**
     * when displaying DPI from for readig purposes
     * hide rest, submit, and add crq buttons
    */
    if(navigation_type == 'history'){
       $("#resetBtn").hide();
       $("#submitBtn").hide();
       $("#add-new-crq").hide();
       //this function will get and display DPI content 
       Get_Dpi_Content();
    }

    /**when displaying DPI from for editing purposes
     * hide rest, and change button display text to 'edit'
    */
    else if(navigation_type == 'modify'){
        $("#resetBtn").hide();
        $("#submitBtn").html("Edit");
        //this function will get and display DPI content 
        Get_Dpi_Content();
    }
    else{
        //calling this function to get and dislay the intial component list for the new DPI
        get_components_list();
        get_additional_crq(1);
    }


    //crq forms counter
    let counter = 1;
    /*click event handler for adding additional crq form*/
    $("a#add-crq").on('click',function(e){
        //prevent reloading page when clicking
        e.preventDefault();
        // increase the counter
        counter++;
        // call this function to get addtional crq
        get_additional_crq(counter); 
    });

    //change event when selecting different app name from dropdown list
    $( ".select-app" ).change(function() {
       //this function will replace/display the new component list 
       get_components_list(counter);
    });

    //Adding setTimeout so the event does not get triggered before the CRQ from is added
    setTimeout(function(){
        
        //change event handler to trigger the change of a checked/unchecked checkbox
        $(document).on('change', 'a.dropdown-item > input[type="checkbox"]', function() {
            
            // olean value set to false by default
            let is_checked = false;
            
            //get the parent container id
            let parent_div = $(this).closest('div.row').attr('id');
            
            //get the checked/unchecked value
            let checked_value = $(this).val();
           
            //if the value is checked
            if(this.checked) {
                //if the checked value is not no components
                if(checked_value != 'No component/s'){
                    // set boolean value to true
                    is_checked = true;
                    // call this function to add the build fields 
                    add_build_field(parent_div, checked_value, is_checked);
                }
                // if checked value is 'no components'
                else
                {
                    //from the parent container, find the checkboxes
                    $("#"+parent_div).find('input[type="checkbox"]').each(function(e){
                        
                        //check for the other values (not 'no components')
                        if($(this).val() != "No component/s"){
                            //uncheck and disable the checkboxes
                            $(this).prop('checked', false);
                            $(this).attr('disabled', true);

                            //set boolean to false and call the function to remove the builds fields
                            is_checked = false;
                            add_build_field(parent_div, $(this).val(), is_checked);
                        }
                    });
                }
            }
            //if the value is unchecked
            else{
                //if the value is not 'no components'
                if(checked_value != 'No component/s'){

                    //set boolean to false and call the function to remove the builds fields
                    is_checked = false;
                    add_build_field(parent_div, checked_value, is_checked);
                }
                 //if the value is 'no components'
                else
                {
                    //from the parent container, find the checkboxes
                    $("#"+parent_div).find('input[type="checkbox"]').each(function(e){
                        
                        //check the other values (not 'no components')
                        if($(this).val() != "No component/s"){
                            
                            //enable the checkboxes
                            $(this).attr('disabled', false);
                        }
                    });
                }// end of if statement
            }// end of if statement
        });//end of event handler
    }, 1000);//end of setTimeout


    // click event handler for delete button
    $(document).on('click', 'button#deleteButton', function() {
        
        // find the main continer id
        let parent_div = $(this).closest('div.row').attr('id');
        
        // remove the continer with all the elements iside the continer 
        $("#"+parent_div).remove();
    });

    //click event fro submit/edit buttn
    $("button#submitBtn").on('click',function(e){
        //prevent reloading page when clicking
        e.preventDefault();
        
        // get all the values entered in each from field 
        let release_id = $("#releaseID").val();
        let app_name = $('#app-opt').find('option:selected').text();
        let release_date = $("#releaseDate").val();
        let coord_name = $("#coordName").val();
        let coord_email = $("#coordEmail").val();
        let release_status = $("#status-opt").find('option:selected').text();

        //check if any field is empty
        if(release_id == null || release_id == "" || app_name == null || app_name == "" ||
            release_date == null || release_date == "" || coord_name == null || coord_name == "" ||
            coord_email == null || coord_email == "" || release_status == null || release_status == "")
        {
            //display a message if any field is empty
            $('p.message').text('One or more fields are empty!');
        }
        else if (!coord_email.includes("@") &&!coord_email.includes("."))
        {
            $('p.message').text('Invalid email Format');
        }
        else{
            // list for all CRQ numbers and their components
            let crq_list = [];
            
            // loop through provided CRQ numbers and their components
            for(let i = 1; i <= counter; i++){
                
                //find CRQ number field and get the value
                let crq = $("#newCRQ"+i).find("#crqNumber"+i).val();
                
                //check for udefined crq continer that was deleted
                if(crq != undefined){

                    if(crq.includes("CRQ") && crq != '' || crq != null){
                        //list for components
                        let crq_comp=[];
                        
                        //find each selected/checked components
                        $("#newCRQ"+i).find('input[type="checkbox"]:checked').each(function(e){
                            
                            // if the value is 'no components'
                            if($(this).val() == 'No component/s'){
                                
                                //push selected component to the list
                                crq_comp.push({
                                    "component": $(this).val(),
                                    "repo_name": '',
                                    "branch_name": '',
                                    "tag": '',
                                    "build": ''
                                })
                            }
                            // if the value is not 'no components'
                            else{
                                // find and get the build, repo, tag and branch values
                                let build_value = $("textarea.newCRQ"+i+"-build-"+$(this).val().split(' ').join('')).val();
                                let repo_value = $("input.newCRQ"+i+"-repo-"+$(this).val().split(' ').join('')).val();
                                let branch_value = $("input.newCRQ"+i+"-branch-"+$(this).val().split(' ').join('')).val();
                                let tag_value = $("input.newCRQ"+i+"-tag-"+$(this).val().split(' ').join('')).val();
        
                                //push selected component to the list
                                crq_comp.push({
                                    "component": $(this).val(),
                                    "repo_name": repo_value,
                                    "branch_name": branch_value,
                                    "tag": tag_value,
                                    "build": build_value
                                });
                            }// end of if statement
                        });// end of find function

                        //push crq number and component list to the crq list
                        crq_list.push(
                            {
                            "crq_number": crq,
                            "project_components": crq_comp 
                            }
                        )
                    }
                }
              
            }//end of for loop

            // convert the content entered in the html form to json format
            let content =  JSON.stringify(
                {
                    "application_name": app_name,
                    "date": release_date,
                    "status": release_status,
                    "release_id": release_id,
                    "coordinate_name" : coord_name,
                    "coordinate_email" : coord_email,
                    "crq_list": crq_list,
                },
                null,
                2
            );

           // if the user navigation is Modify DPI
            if(navigation_type == 'modify'){
                //call this function and pass content, release id, and api call type (PUT)
                Save_DPI_Content(content, "PUT", release_id, app_name);
            }
           // if the user navigation is New DPI
            else{
                //call this function and pass content, release id, and api call type (GET)
                Save_DPI_Content(content, "POST", release_id, app_name);
            }
        }//end of if statement
    });//end of click event
});


