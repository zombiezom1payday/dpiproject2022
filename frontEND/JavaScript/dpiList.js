/** 
 * this file contains the functionality for DPI files list
 * 
 * it will be linked to dpiList.html
*/
$(document).ready(function(){
   
    /**
     * Handles the search button function.
    */
    $("#search_Textbox").keyup(function(event) {
        if (event.keyCode === 13) {
            $("#searchBtn").click();
        }
    });

    /**
     * handle te search button click 
    */
    $('#searchBtn').on('click',function(e) {
        e.preventDefault();
        // get the search value
        searchValue = document.getElementById('search_Textbox').value;
        
        // call his function to get the search result
        get_search_result(searchValue); 
    });

    //this function will get and display a paginated list of files
    get_files();

    /**
     * click event for each file in the list
     */
    $('#list').on('click', 'a.file', function(e){
        //prevent default reload
        e.preventDefault();
        //get the clicked file name and path
        let file_name = e.currentTarget.innerText;
       //store them in a local storage o be used in dpi1.js and dpi2.js
        localStorage.setItem('file-name', file_name);
        //redirect to DPI frm page
        window.location.href = "../Html/dpi.html";
    });

    //pagination variable - to hold page number
    let page = 1;
    //previous arrow click event
    $('a#prev-link').on('click',function(e){
        e.preventDefault();
        //decrease page number
        page--;

        //if page number is 0 or less, keep it 1
        if(page <= 0){
            page = 1;
            //this function will get the files list that corresponded to the page number
            get_files(page);
        }
        else{
            //this function will get the files list that corresponded to the page number
            get_files(page);
        }
    });

    //next arrow click event
    $('a#next-link').on('click',function(e){
        e.preventDefault();
        //increase page number
        page++;
        //this function will get the files list that corresponded to the page number
        get_files(page);
    });
});

//this function will get the files list that corresponded to the page number
function get_files(page=1) {
    //get and display the app name from apps.js
    let app_name = localStorage.getItem('app-name');
    $("#app-name").text(app_name);

    //get the main part of api url, branch name and projects IDs from globalVars.js
    let urlMain = localStorage.getItem('urlMain');
    let branch = localStorage.getItem('branch_name');
    const proj_IDs =  JSON.parse(localStorage.getItem('proj_IDs'));
    let projId = 0;

    // find the project id equal to the selected app
    for (let app of Object.keys(proj_IDs)) {
        if(app == app_name){
            projId = proj_IDs[app];
        }
    }

    // ajax call to gitlab api 
    $.ajax({
        url: urlMain+ projId + "/repository/tree?ref="+branch+"&per_page=10&page="+page,
        method: "GET",
        async: false,
        success: function(data) {
            //if successed, get the list container
            let list = document.getElementById("list");
            //variable to hold html elements
            let items = "";
            //loop thrugh the data
            for (let i = 0; i < data.length; i++){
                if(data[i].name.includes('.json'))
                {
                    //create html element with file name and path
                    items += "<li class='list-group-item' ><a href='#' class='file'> <span hidden>" + data[i].path +  "</span>"+ data[i].name + "</a></li>";
                }
            }

            //if some data were return
            if(data.length >= 1){
                //change the page number and display the files
                let page_num = $("#page-num");
                page_num.text(page);
                list.innerHTML = items;
            }
        },
        error: function(request, msg, error){
            //display a message if the api call did not successed
            $('p.message').append('Something went wrong while uploading DPI to GitLab!, Contant the admin');
        }
    })
}


/**
 * Handles the SEARCH call of the api, uses a GET than an IF to handle this.
 * @param {} searchTerm 
*/
function get_search_result(searchTerm) {
    //get the app name from apps.js
    let app_name = localStorage.getItem('app-name');

    //get the main part of api url, branch name and projects IDs from globalVars.js
    let urlMain = localStorage.getItem('urlMain');
    let branch = localStorage.getItem('branch_name');
    const proj_IDs =  JSON.parse(localStorage.getItem('proj_IDs'));
    let projId = 0;

    // find the project id equal to the selected app
    for (let app of Object.keys(proj_IDs)) {
        if(app == app_name){
            projId = proj_IDs[app];
        }
    }

    // GET api call
    $.ajax({
        url: urlMain+projId+"/repository/tree?ref="+branch,
        method: "GET",
        async: false,
        success: function(data) { 
    
            let items = "";
            //loop through the result data
            for (let i = 0; i < data.length; i++){

                //check if the result data is simialar to the search item
                if(data[i].name.includes(searchTerm)) { 
                    //if it matches the search term, than add to list.
                    items += "<li class='list-group-item file' ><a href='#'> <span hidden>" + data[i].path +  "</span>"+ data[i].name + "</a></li>";
                }
            }
            //ad the new elemets to the html list
            list.innerHTML = items;
        },
        error: function(request, msg, error){ //standard error log.
            $('p.message').text('Something went wrong while searchig, please contact the Admin!');
        }
    })
}